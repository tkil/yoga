export interface IGroupMeta {
  group: string
  groupParent: string | null
  sorting: number
}

export interface IItem {
  description: string
  name1: string
  name2: string
  teacher: string
  url: string
}

export interface IItemRaw extends IItem {
  group: string
  sorting: number
}

export interface ItemGroupTree {
  group: string
  items: IItem[]
  subGroups: ItemGroupTree[]
}
