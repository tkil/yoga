// import 'unfetch/polyfill'
// import { configure } from 'enzyme'
// import Adapter from 'enzyme-adapter-react-16'
// import { log } from '@utils/logger'
// import matchers from './matchers'

// configure({ adapter: new Adapter() })

// Custom matchers
// expect.extend(matchers)

// Mock logger for all test because of all the noise
// Each test can grab the mocks for assertions if needed
// jest.mock('@utils/logger') // Will use src/utils/__mocks__/logger.ts
// beforeEach(() => {
//   (log as any).persist.mockClear();
//   (log as any).debug.mockClear();
//   (log as any).info.mockClear();
//   (log as any).warn.mockClear();
//   (log as any).error.mockClear()
// })

// Setup for match media to work with jest
// window.matchMedia =
//   window.matchMedia ||
//   (() => {
//     return { matches: false, addListener: () => null, removeListener: () => null }
//   })

// const originalWarn = window.console.warn
// window.console.warn = (...args: any) => {
//   if (!/^Warning:\ component/.test(args[0] as string)) {
//     originalWarn(...args)
//   }
// }

// Object.defineProperty(window, 'innerWidth', { writable: true, configurable: true, value: 320 })
// Object.defineProperty(window.navigator, 'clipboard', { value: { writeText: jest.fn() } })
