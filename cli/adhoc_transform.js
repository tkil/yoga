var fs = require('fs');

const mapResource = (r, k) => ({
  cagegory1: /Flow/.test(r.category1) ? 'Flow' : r.category1,
  cagegory2: /Flow/.test(r.category1) ? r.category1.split('ute Flows')[0]: null,
  description: "",
  name1: r.title.split(' - ')[0],
  name2:  r.title.split(' - ')[1] || null,
  sorting: k,
  teacher: r.teacher,
  url: r.url
})

const main = () => {
  const files = fs.readdirSync('.')
  const items = files.reduce((acc, f) => {
    if(/\.json/.test(f) && f !== 'grouping.json' && f !== 'resources.json') {
      const fileData = JSON.parse(fs.readFileSync('./' + f, 'utf8'))
      const result = fileData
        .resources
        .map((r) => ({ category1: fileData.title, ...r}))
        .map(mapResource)
      return [
        ...acc,
        ...result
      ]
    }
    return acc
  }, [])
  console.log(items)
  fs.writeFileSync('./resources.json', JSON.stringify(items))
}

main()