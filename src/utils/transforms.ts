import { IGroupMeta, IItem, IItemRaw, ItemGroupTree } from 'types'

export const itemsRawToItems = (irs: IItemRaw[]): IItem[] => irs.map((ir) => ({
  description: ir.description,
  name1: ir.name1,
  name2: ir.name2,
  teacher: ir.teacher,
  url: ir.url
}))

export const itemsToGroupTrees = (itemsRaw: IItemRaw[], groupMetas: IGroupMeta[]): ItemGroupTree[] => {
  const parentGroups = groupMetas.filter((g) => g.groupParent === null)
  const gTrees: ItemGroupTree[] = parentGroups.map((g) => {
    const divedGroups: IGroupMeta[] = groupDive(groupMetas, g.group)
    const subGroups = itemsToGroupTrees(itemsRaw, divedGroups)
    const items = itemsRawToItems(itemsRaw.filter((ir) => ir.group === g.group))
    const gt: ItemGroupTree = {
      group: g.group,
      items,
      subGroups
    }
    return gt
  })
  return gTrees
}

export const groupDive = (groupMetas: IGroupMeta[], parent: string): IGroupMeta[] => {
  const dived = groupMetas
    .filter((gm) => gm.groupParent !== null)
    .map((gm) => ({
      ...gm,
      groupParent: gm.groupParent === parent ? null : gm.groupParent
    }))
  return dived.find((g) => g.groupParent === null) !== undefined
    ? dived
    : []
}

