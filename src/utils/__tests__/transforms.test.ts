// Project
import {
  IGroupMeta,
  IItem,
  IItemRaw
} from 'types'
import { groupDive, itemsToGroupTrees, itemsRawToItems } from '@utils/transforms'

describe('Util Transforms:', () => {
  describe('itemsRawToItems', () => {
    it.only('should transform to items', () => {
      // Arrange
      const raws: IItemRaw[] = [{
        description: 'description',
        group: 'group',
        name1: 'name1',
        name2: 'name2',
        sorting: 0,
        teacher: 'teacher',
        url: 'url',
      }]
      // Act
      const items: IItem[] = itemsRawToItems(raws)
      // Assert
      const expected = [{
        description: 'description',
        name1: 'name1',
        name2: 'name2',
        teacher: 'teacher',
        url: 'url',
      }]
      expect(items).toEqual(expected)
    })
  })
  describe('groupDive', () => {
    it.only('should transform to items', () => {
      // Arrange
      const groupMeta: IGroupMeta[] = [
        {
          group: 'a',
          groupParent: null,
          sorting: 0
        },
        {
          group: 'b',
          groupParent: 'papa',
          sorting: 0
        },
        {
          group: 'c',
          groupParent: 'papa',
          sorting: 0
        },
        {
          group: 'd',
          groupParent: 'other',
          sorting: 0
        }
      ]
      const parent = 'papa'
      // Act
      const dived: IGroupMeta[] = groupDive(groupMeta, parent)
      // Assert
      const expected = [
        {
          group: 'b',
          groupParent: null,
          sorting: 0
        },
        {
          group: 'c',
          groupParent: null,
          sorting: 0
        },
        {
          group: 'd',
          groupParent: 'other',
          sorting: 0
        }
      ]
      expect(dived).toEqual(expected)
    })
  })
  describe('itemsToGroupTree', () => {
    it.only('should transform to items', () => {
      // Arrange
      const groupMeta: IGroupMeta[] = [
        {
          group: 'a',
          groupParent: null,
          sorting: 0
        },
        {
          group: 'papa',
          groupParent: null,
          sorting: 0
        },
        {
          group: 'c',
          groupParent: 'papa',
          sorting: 0
        },
        {
          group: 'd',
          groupParent: 'other',
          sorting: 0
        }
      ]
      const items: IItemRaw[] = [
        {
          group: "a",
          description: "",
          name1: "item a1",
          name2: "",
          sorting: 0,
          teacher: "",
          url: ""
        },
        {
          group: "a",
          description: "",
          name1: "item a2",
          name2: "",
          sorting: 0,
          teacher: "",
          url: ""
        },
        {
          group: "b",
          description: "",
          name1: "item b1",
          name2: "",
          sorting: 0,
          teacher: "",
          url: ""
        },
        {
          group: "d",
          description: "",
          name1: "item d1",
          name2: "",
          sorting: 0,
          teacher: "",
          url: ""
        }
      ]
      // Act
      const dived = itemsToGroupTrees(items, groupMeta)
      // Assert
      const expected = [
        {
          group: "a",
          items: [
            {
              description: "",
              name1: "item a1",
              name2: "",
              teacher: "",
              url: ""
            },
            {
              description: "",
              name1: "item a2",
              name2: "",
              teacher: "",
              url: ""
            }
          ],
          subGroups: []
        },
        {
          group: "papa",
          items: [],
          subGroups: [
            {
              group: "c",
              items: [],
              subGroups: []
            }
          ]
        }
      ]
      expect(dived).toEqual(expected)
    })
  })
})