// NPM
import { html, render } from 'lit-html'
// Project
import '@components/register'
import itemData from "./data/items.json"
import groupData from "./data/groups.json"
import { itemsToGroupTrees } from '@utils/transforms'

import '@styles/main.css'

const main = () => {
  const app = document.getElementById('app')
  if (app) {
    const groupTrees = itemsToGroupTrees(itemData, groupData)
    const groups = groupTrees
      .map((gt) => html`
        <resource-group item-group="${JSON.stringify(gt)}"></resource-group>
      `)
    render(groups, app)
  }
}

document.addEventListener("DOMContentLoaded", main);
