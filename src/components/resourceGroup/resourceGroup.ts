import { html, render } from 'lit-html'
import { IItem, ItemGroupTree } from "types"

const componentName = 'resource-group'

// "sourLemon": "#ffeaa7",
// "brightYarrow": "#fdcb6e"

const style = /*css*/`
  .group-header {
    margin: 0;
  }
  section {
    border: 3px solid #3498DB;
    margin: 16px; 
    padding: 8px;
    border-radius: 8px;
  }
  resource-group {
  }
`

const templateSubGroups = (subGroups: ItemGroupTree[]) => subGroups.map((sg) => html`
  <resource-group item-group="${JSON.stringify(sg)}"></resource-group>
`)

const templateResourceLinks = (is: IItem[]) => {
  return is.map((i) =>
    html`<resource-link
      description="${i.description}"
      name1="${i.name1}"
      name2="${i.name2}"
      teacher="${i.teacher}"
      url="${i.url}"
    ></resource-link>`
  )
}

const template = (itemGroup: ItemGroupTree) => {
  return html`
    <style>${style}</style>
    <section>
      <h2 class="group-header">${itemGroup.group}</h2>
      ${templateResourceLinks(itemGroup.items)}
      ${templateSubGroups(itemGroup.subGroups)}
    </section>
  `
}

const safeJsonParse = (str: string | null): ItemGroupTree => {
  try {
    return JSON.parse(str || '')
  } catch(err) {
    return {
      group: '',
      items: [],
      subGroups: []
    }
  }
}

window.customElements.define(componentName, class extends HTMLElement {
  private _shadow: ShadowRoot
  private _itemGroup: ItemGroupTree

  constructor() {
    super()
    this._shadow = this.attachShadow({mode: 'open'})
    // Attribute Mapping
    this._itemGroup = safeJsonParse(this.getAttribute('item-group'))
  }

  public connectedCallback() {
    render(template(this._itemGroup), this._shadow)
  }

  static get observedAttributes() {
    return ['item-group']
  }

  public attributeChangedCallback(attrName: string, oldVal: string, newVal: string)	{
    switch (attrName) {
      case 'item-group':
        this._itemGroup = safeJsonParse(this.getAttribute('item-group'))
    }
    render(template(this._itemGroup), this._shadow)
  }
})
