import { html, render } from 'lit-html'

const componentName = 'resource-link'

const style = /*css*/`
  a {
    box-sizing: border-box;
    display: block;
    margin: 5px 2%;
    background-color: #ffeaa7;
    border: 2px solid #fdcb6e;
    padding: 8px;
    border-radius: 8px;
    cursor: pointer;
    text-decoration: none;
    color: black;
    height: 100%;
  }
  .container {
  }
  span + span {
    border-left: 4px solid #fdcb6e;
    margin-left: 4px;
    padding-left: 8px;
  }
  .teacher {
    font-style: italic;
  }
`

interface ITemplateArgs {
  description: string
  name1: string
  name2: string
  teacher: string
  url: string
}
const template = (args: ITemplateArgs) => html`
  <style>${style}</style>
  <a href="${args.url}">
    <div class="container">
      <span>${args.name1}</span>
      ${args.name2 && html`<span>${args.name2}</span>`}
      <span class="teacher">${args.teacher}</span>
    </div>
  </a>
`

window.customElements.define(componentName, class extends HTMLElement {
  private _shadow: ShadowRoot
  private _description: string
  private _name1: string
  private _name2: string
  private _teacher: string
  private _url: string
  constructor() {
    super()
    this._shadow = this.attachShadow({mode: 'open'})
    // Attribute Map
    this._description = this.getAttribute('description') || ''
    this._name1 = this.getAttribute('name1') || ''
    this._name2 = this.getAttribute('name2') || ''
    this._teacher = this.getAttribute('teacher') || ''
    this._url = this.getAttribute('url') || ''
  }

  connectedCallback() {
    render(template({
      description: this._description,
      name1: this._name1,
      name2: this._name2,
      teacher: this._teacher,
      url: this._url
    }), this._shadow)
  }

  static get observedAttributes() {
    return ['description', 'name1', 'name2', 'teacher', 'url'];
  }

  attributeChangedCallback(attrName: string, oldVal: string, newVal: string)	{
    switch(attrName) {
      case 'description':
        this._description = newVal
        return
      case 'name1':
        this._name1 = newVal
        return
      case 'name2':
        this._name2 = newVal
        return
      case 'teacher':
        this._teacher = newVal
        return
      case 'url':
        this._url = newVal
        return
    }
  }
})